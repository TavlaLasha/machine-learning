import os
import random as rd
import sys


directory = "myFilesFor1"
Ns = input("N: ")

N = 0
if Ns != "":
    N = int(Ns)
else:
    sys.exit()

numsString = ""
for i in range(1, N+1):
    numsString += str(round(rd.uniform(1, 10), 2)) + "-"

numsString = numsString[:-1]
print(numsString)
numbers = list(map(float, numsString.split("-")))
print(numbers)

if not os.path.isdir(directory):
    os.mkdir(directory)

f = open("{}/data.txt".format(directory), "w")

for x in numbers:
    y = 3 * x - 2
    f.write("x: {}, y: {}\n".format(str(x), str(y)))


