import sys

string = input("N: ")
if string == "":
    sys.exit()

Vowels = "AEIOUaeiou"

VowelCount = 0
ZeroCount = 0
FinalString = ""
FinalNumbers = ""

for char in string:
    if char.isalpha():
        FinalString += char
        if char in Vowels:
            VowelCount += 1
    elif char.isdigit():
        FinalNumbers += char
        if char == "0":
            ZeroCount += 1

print("{} - {}; {} - {}".format(FinalString, VowelCount, FinalNumbers, ZeroCount))

