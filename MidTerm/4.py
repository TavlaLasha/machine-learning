import numpy as np
from scipy import stats as st

a = 0
b = 100

m1 = np.random.randint(low=a, high=b, size=(10, 10))
m2 = np.random.randint(low=a, high=b, size=(10, 10))

CombinedList = np.concatenate((m1.flatten(), m2.flatten()))
print(CombinedList)

m1Mode = st.mode(m1, axis=None)[0]
m2MaxMinusMin = np.max(m2) - np.min(m2)

print("First Matrix Mode: {}; Second Matrix Max-Min= {}".format(m1Mode, m2MaxMinusMin))
