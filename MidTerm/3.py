import os
from datetime import datetime, timedelta, date

import numpy as np

directory = "myFilesFor3"
if not os.path.isdir(directory):
    os.mkdir(directory)
f = open("{}/data.txt".format(directory), "w")

nums = np.random.randint(low=1000, high=9999, size=100)
FinalNumsData = str(nums).replace("[", "").replace("]", "").replace("\n", "")
f.write(FinalNumsData)


def random_date_generator(start_date, end_date):
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = np.random.randint(days_between_dates)
    random_date = start_date + timedelta(days=random_number_of_days)
    return random_date


FinalDatesData = "\n"
for i in range(1, 101):
    FinalDatesData += str(random_date_generator(date(2019, 1, 1), date(2019, 1, 31))) + " "

f.write(FinalDatesData.rstrip())
f.close()

# Reading

R = open("{}/data.txt".format(directory), "r")
FileData = R.read().split('\n')

Numbers = np.array(FileData[0].split(' '))
Numbers = Numbers.astype(int)
Dates = np.array(FileData[1].split(' '))
Dates = Dates.astype(datetime)

print("Min Date: {}; Max Number: {}".format(np.min(Dates), np.max(Numbers)))
