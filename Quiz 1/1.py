import random as rd

myDict = {}
randNum = rd.sample(range(1, 100), 10)
maxNum = 0
maxNumKey = 0
for i in range(0, 10):
    choice = randNum[i]
    result = 0
    for c in str(choice):
        result += int(c)
        myDict[choice] = result
        if result > maxNum:
            maxNum = result
            maxNumKey = choice

print(myDict)
print("Highest value element => {}: {}".format(maxNumKey, maxNum))
