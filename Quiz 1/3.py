import numpy

def HasOnlyTwoDivisors(n):
    result = set()

    for i in range(1, int(n**0.5) + 1):
        if n % i == 0 and i not in [1, n]:
            result.add(i)
            result.add(n//i)
            if len(result) > 2:
                break

    return len(result) == 2


FinalList = []
for num in range(10, 501):
    if HasOnlyTwoDivisors(num):
        FinalList.append(num)

print("10 - 500 შუალედიდან, მხოლოდ ორი გამყოფი თავის თავის და 1-ის გარდა აქვთ შემდეგ რიცხვებს:")
print(FinalList)
