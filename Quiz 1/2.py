import random as rd

mylist = []
for i in range(20):
    mylist.append(rd.randint(5, 15))
print(mylist)

count = 0
save = ''
thisdict = {i: mylist.count(i) for i in mylist}
biggests = {}
for key in thisdict.keys():
    if thisdict[key] >= count:
        count = thisdict[key]
        if save != key and count not in biggests.values():
            biggests.clear()
        biggests[key] = count
        save = key
    print("{} გამეორდა {}-ჯერ".format(key, thisdict[key]))

print("\nყველაზე ხშირად გამეორდა: ")
for i in biggests:
    print("{} => {}-ჯერ".format(i, biggests.get(i)))
