d = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}

key = int(input("Input key to find: "))

print("Key {} in out dictionary".format("IS" if key in d else "IS NOT"))
