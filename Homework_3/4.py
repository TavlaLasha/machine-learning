import os
directory = "myFiles2"

if not os.path.isdir(directory):
    os.mkdir(directory)

for i in range(1, 31):
    f = open("{}/file{}.txt".format(directory, i), "w")
    f.write("Programmer{} ".format(i))
    f.close()
