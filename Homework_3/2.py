import os

directory = "myFiles"

if not os.path.isdir(directory):
    os.mkdir(directory)

f = open("{}/data1.txt".format(directory), "w")
numCount = 100
for i in range(numCount+1):
    if i == numCount:
        f.write(str(i))
        continue
    f.write("{}, ".format(str(i)))
f.close()
