import statistics

numbs = [5, 2, 5, 4, 5, 6, 9, 8, 9, 10]


def median(lst):
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2

    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1]) / 2.0


mode = max(set(numbs), key=numbs.count)
median = median(numbs)

print("Mode: {}, Median: {}, Average: {}".format(mode, median, sum(numbs) / len(numbs)))
