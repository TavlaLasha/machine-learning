def checkIfOddOrEven(x):
    return False if x % 2 == 0 else True


print(1, checkIfOddOrEven(1))
print(2, checkIfOddOrEven(2))
print(3, checkIfOddOrEven(3))
print(5, checkIfOddOrEven(5))
print(6, checkIfOddOrEven(6))
