numbs = [1, 2, 3, 4, 5]

numbsSum = 0
numbsMax = 0
numbsMin = numbs[0]

for i in numbs:
    if i > numbsMax:
        numbsMax = i
    if i < numbsMin:
        numbsMin = i
    numbsSum += i

numbsAvg = numbsSum / len(numbs)
print("Min: {}; Max: {}; Average: {}".format(numbsMin, numbsMax, numbsAvg))


def bubbleSort(lst):
    for iter_num in range(len(lst) - 1, 0, -1):
        for idx in range(iter_num):
            if lst[idx] > lst[idx + 1]:
                temp = lst[idx]
                lst[idx] = lst[idx + 1]
                lst[idx + 1] = temp


numbs.append(102)
numbs.insert(0, 205)
numbs.pop(3)
bubbleSort(numbs)
print(numbs)



