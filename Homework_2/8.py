import random


def calculateAVG(x, y):
    if not x or not y:
        return
    print("x: {}; y: {}; AVG: {}".format(x, y, (x + y) / 2))


for i in range(3):
    calculateAVG(random.randint(1, 100), random.randint(1, 100))
