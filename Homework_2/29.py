sentence = "python php pascal javascript java"
sentenceList = sentence.split(' ')

print(sentenceList)
result = sentenceList[0]

for i in sentenceList[1:]:
    if len(result) < len(i):
        result = i

print("Longest word: ", result, ". It has {} characters".format(len(result)))
