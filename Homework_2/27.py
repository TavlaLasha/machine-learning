numbs = [1, 5, 23, 5, 12, 2, 5, 1, 18, 5]
occurrences = {}

for i in numbs:
    if i not in occurrences.keys():
        occurrences[i] = 1
    else:
        occurrences[i] += 1

print(sorted(occurrences.items(), key=lambda item: item[1], reverse=True))
