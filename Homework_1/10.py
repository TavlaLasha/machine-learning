x = input("შეიყვანეთ წელიწადი: ")
if x != "":
    year = int(x)

    if (year % 400 == 0) and (year % 100 == 0):
        print("{0} წელი ნაკიანია".format(year))
    elif (year % 4 ==0) and (year % 100 != 0):
        print("{0} წელი ნაკიანია".format(year))
    else:
        print("{0} წელი არ არის ნაკიანი".format(year))
else:
    print("წელიწადი არ მიგვიღია :(")
