import math

x = input("პირველი რიცხვი: ")
y = input("მეორე რიცხვი: ")

if x != "" and y != "":
    xVal = int(x)
    yVal = int(y)
    if xVal > 0 and yVal > 0:
        print("უდიდესი საერთო გამყოფი: ", math.gcd(xVal, yVal))
    else:
        print("ორივე რიცხვი უნდა იყოს დადებითი (0-ზე მეტი)")
else:
    print("ორივე რიცხვი სავალდებულოა!")
