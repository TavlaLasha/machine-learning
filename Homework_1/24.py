x = input("შეიყვანეთ რიცხვი: ")

if x != "":
    xVal = int(x)
    result = "{}-ის გამყოფებია: ".format(xVal)
    for i in range(1, xVal+1):
        if xVal % i == 0:
            result += "{}, ".format(i)
    result = result[:-2]
    print(result)
else:
    print("რიცხვი არ მიგვიღია :(")
