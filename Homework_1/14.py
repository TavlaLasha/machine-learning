x = 0
nullCount = 0
for i in range(10):
    inp = input("შეიყვანეთ რიცხვი {}: ".format(i+1))
    x += float(inp) if inp != "" else 0
    nullCount += 1 if inp == "" else 0

if 10 - (nullCount+1) < 0:
    print("არცერთი რიცხვი არ მიგვიღია :(")
else:
    print("საშუალო არითმეტიკული: ", x/(10-nullCount))
