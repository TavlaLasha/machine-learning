num = input("შეიყვანეთ რიცხვი: ")

if num != "":
    num = int(num)
    temp = num
    rev = 0
    while num > 0:
        dig = num % 10
        rev = rev * 10 + dig
        num = num // 10
    if temp == rev:
        print("რიცხვი არის პალინდრომი")
    else:
        print("რიცხვი არ არის პალინდრომი")
else:
    print("რიცხვი არ მიგვიღია :(")
