from scrapy.http import TextResponse
import requests
import pprint
import pandas as pd
import re

base_url = "https://www.yelp.com"
# detail_urls = []

names = []
categories = []
ratings = []
review_counts = []

for i in range(240, 400, 10):
    url = requests.get(
        base_url + "/search?find_desc=&find_loc=San+Francisco%2C+CA&start={}".format(i))
    response = TextResponse(url.url, body=url.text, encoding='utf-8')
    # print(url.text)
    detail_urls = response.css('div.businessName__09f24__EYSZE div h3 span a::attr(href)').getall()
    pprint.pprint(detail_urls)

    if len(detail_urls) == 0:
        break

    for url in detail_urls:
        if url.find("/biz/") != -1:
            full_http_path = requests.get(base_url + url)

            details_page_response = TextResponse(full_http_path.url, body=full_http_path.text, encoding='utf-8')

            item_name = details_page_response.css("div.headingLight__09f24__N86u1 h1::text").get()
            item_main_categories = details_page_response.css("div.photo-header-content__09f24__q7rNO div div span.margin-r1-5__09f24__ot4bd span a::text").extract()
            item_rating = details_page_response.css("div.photo-header-content__09f24__q7rNO div div div.gutter-1-5__09f24__vMtpw div.border-color--default__09f24__NPAKY span div::attr(aria-label)").get()
            item_reviews = details_page_response.css("div.photo-header-content__09f24__q7rNO div div div.gutter-1-5__09f24__vMtpw div.arrange-unit-fill__09f24__CUubG span a::text").get()
            # pprint.pprint()

            if type(item_name) is str and len(item_name) != 0:
                print(item_name)
                names.append(item_name)
                categories.append(item_main_categories[0])
                rating = re.findall(r"[-+]?\d*\.\d+|\d+", item_rating)
                ratings.append(float(rating[0]))
                review_count = int(''.join(filter(str.isdigit, item_reviews.split(" ")[0])))
                review_counts.append(review_count)

data = {
    "name": names,
    "category": categories,
    "rating": ratings,
    "review_count": review_counts
}
df = pd.DataFrame(data)
print(df)
df.to_excel("yelp.xlsx", index=False)
