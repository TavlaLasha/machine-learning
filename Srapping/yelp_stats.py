import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_excel('yelp.xlsx')

categories = []
rating_values = []
reviews = []
for item in data.to_dict(orient='records'):
    if item['category'] in categories:
        index = categories.index(item['category'])
        rating_values[index].append(item['rating'])
        reviews[index].append(item['review_count'])
    else:
        categories.append(item['category'])
        rating_values.append([item['rating']])
        reviews.append([item['review_count']])

# print(rating_values)

# ratings = []
# for i in rating_values:
#     ratings.append(np.average(i))

print(len(categories), categories)
print(len(rating_values), rating_values)
print(len(reviews), reviews)

markers = ["*", ".", "v", "+", "x", "X"]
marker = markers[0]
for i in range(len(categories)):
    if i % 10 == 0:
        marker = markers[int(i / 10)]
    plt.scatter(rating_values[i], reviews[i], marker=marker, label=categories[i])
plt.legend(ncol=5, bbox_to_anchor=(0.8, -0.055))
plt.subplots_adjust(top=0.99, bottom=0.3)
plt.show()
