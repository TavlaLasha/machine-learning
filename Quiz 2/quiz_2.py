import numpy as np
import pandas
from numpy import random as nRnd
import matplotlib.pyplot as plt

FilePath = "Files/data.xlsx"
WorkersCount = 500

# ---1
Workers = nRnd.randint(low=100, high=999, size=WorkersCount)

df1 = pandas.DataFrame(Workers)
ExWriter = pandas.ExcelWriter(FilePath, engine="openpyxl", mode="w")
df1.to_excel(ExWriter, startcol=0, header=False, index=False)

# ---2
for i in range(1, 6):
    WorkHours = np.round(nRnd.uniform(low=0, high=10.00, size=WorkersCount), 2)
    df2 = pandas.DataFrame(WorkHours)
    df2.to_excel(ExWriter, startcol=i, header=False, index=False)

ExWriter.close()

# ---3
ExcelFile = pandas.read_excel(FilePath, engine="openpyxl", header=None)
ChosenWorker = ExcelFile.loc[nRnd.randint(0, WorkersCount-1)]
ChosenWorkerID = ChosenWorker[0]
ChosenWorkerWorkHours = ChosenWorker[1:]

# print(ChosenWorkerID)
# print(ChosenWorkerWorkHours)

x = list(range(1, 6))
plt.scatter(x, ChosenWorkerWorkHours)
plt.xticks(np.arange(min(x), max(x)+1, 1.0))
plt.show()


# ---4
newExcel = ExcelFile.drop(columns=ExcelFile.columns[0])
dataArr = newExcel.to_numpy()
# print(dataArr)
avg = np.average(dataArr)
std = np.std(dataArr)
minH = np.min(dataArr)
maxH = np.max(dataArr)
print("AVG: {}, STD: {}, MIN {}, MAX: {}".format(avg, std, minH, maxH))
